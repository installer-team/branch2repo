# Debian pipeline for Debian-Installer udebs

Build udebs into a test mini.iso image on every push.

## Introduction

This builds on top of the Salsa CI Team's [pipeline](https://salsa.debian.org/salsa-ci-team/pipeline/).

The idea here is that an `aptly` job in the pipeline
allows debian-installer to build a mini.iso
that is able to draw from that APT repository
both at build time, and when the mini.iso is used.

The `aptly` job can draw from additional repositories
by listing them in `BRANCH2REPO_EXTRA_PROJECTS` (see below).

There is a
[talk from Debconf21](https://debconf21.debconf.org/talks/30-branch2repo-enabling-casual-contributions-to-debian-installer/)
that describes branch2repo
(much of the complexity described there has since been streamlined,
by adding features to  `anna` and `net-retriever`).

## Basic Use

It is possible to get default pipeline behaviour
by setting the project's `CI/CD configuration file`
to be `trigger_b2r.yml@installer-team/branch2repo`,
which refers to a file kept in
the installer-team/branch2repo repository.

(BTW If wanting to run tests, one can specify an alternative branch
e.g.: `trigger_b2r.yml@installer-team/branch2repo:testing`)

However having this done in the gitlab settings alone
hides what is going on, so if you are sharing the repo.
it is recommended that you set the `CI/CD configuration file`
as the usual `debian/salsa-ci.yml` and then create a file like this:

```yaml
---
include:
  - https://salsa.debian.org/installer-team/branch2repo/raw/main/trigger_b2r.yml

workflow:
  rules:
    # set the branch name here \v/ to match your feature branch, for easier testing
    - if: $CI_COMMIT_BRANCH == 'my_test_branch'
      variables:
        OQA_JOBSTOCLONE: 'nonGUI@amd64'
        #OQA_CLONE_EXTRA_ARGS: 'GRUBADD=pkgsel/include=netplan-gemerator'
        # useful args for speeding up testing: '_SKIP_POST_FAIL_HOOKS=1 NO_UEFI_POST=1'
        #BRANCH2REPO_DISABLE_MAKEISO: 0
        # For marshalling multiple packages for testing together:
        #BRANCH2REPO_EXTRA_PROJECTS: 'philh/net-retriever:extra-udeb-repo philh/anna:de-dup'
    - when: always

#variables:
  ### b2r enables aptly, so uncoment this if you don't want that
  #SALSA_CI_DISABLE_APTLY: 1
  ### b2r disables these, so uncomment the ones you want (useful for combined deb/udeb packages)
  #SALSA_CI_DISABLE_REPROTEST: 0
  #SALSA_CI_DISABLE_LINTIAN: 0
  #SALSA_CI_DISABLE_PIUPARTS: 0
  #SALSA_CI_DISABLE_BLHC: 0

```

The variables are up to you,
but the above are a good starting place for most udeb packages.

## Variable descriptions:

### BRANCH2REPO_EXTRA_PROJECTS

This allows one to specify extra projects
that will be bundled into the repo being created by the aptly job.

When using this in conjunction with BRANCH2REPO_STATEFUL=1,
if you ever add something here and wish you hadn't,
then simply removing this setting doesn't help
because it will continue to be remembered by the saved state.
In that case, one can set BRANCH2REPO_RESET_PROJECT (once)
to discard the erroneous state.
  
The `trigger_b2r.yml` file includes
the default Debian recipe from the Salsa CI Team's pipeline.
Some of the jobs it defines are not really suitable for udebs,
so `trigger_b2r.yml` disables them by default.
Of course, if you get e.g. lintian to run without errors,
you can re-enable it by uncommenting
the relevant `..._DISABLE_...: 0` line in the example above.

### OQA_JOBSTOCLONE

This is a space separated list of jobs
that should be cloned in order to test
the mini.iso image that is created
with the modifications you specify.
You can either use the numerical job id
(as seen in the URL when looking at the job via the openQA WebUI),
or you can specify a lookup pattern in the form:

    [<group name>/][<test_name>][@<machine_name>]

e.g. `Debian%20(amd64)/kde@amd64`

A search is performed for the most recent job
that succeeded, and matches the spec.,
and if that gives a useful result, it is used.

As a special case, if you leave out the group name,
and specify a machine of either `amd64` or `uefi`
you get a machine name of `Debian%20($machine)` set for you,
so `kde@amd64` is equivalent to the above.

### BRANCH2REPO_DISABLE_MAKEISO

This disables the trigger that normally launches
a `D-I` job to build the mini.iso
(it still makes the aptly repo,
so if you have a mini.iso that's willing to load from there,
that should be enough for testing downloaded udebs)

### (deprecated) BRANCH2REPO_STATEFUL

_see [salsa-branch2repo-state] description below_

If this is set, it causes the
`update-b2r-state`, `harvest-repos`, `aptly-plus`, and `D-I (incl. harvest)`
jobs to be run.

These are mostly needed to get round the fact that
one cannot specify optional `needs:` relationships in gitlab's .yml files,
which makes things seem a bit more complicated than they really are.

These jobs record the fact that a repo has been run in stateful mode,
gather the artifacts together from the other repos,
build an APT repository from the gathered artifacts,
and trigger D-I to build a mini.iso.

N.B. pipelines running in the `master` or `main` branch are treated as a special case,
where state will not be recorded, even is `BRANCH2REPO_STATEFUL` is set.
This is to avoid unintentionally picking up a random selection of unreleased patches
(from other `main` branches in projects where others are working).

## What does this _pipeline_ provide for my project/package?

For most of the elements of the pipeline
that occur before the `Downstream: D-I` stage,
refer to the [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) documentation.

You may see these additional jobs:

#### harvest-repos

If either `BRANCH2REPO_STATEFUL=1` or `BRANCH2REPO_EXTRA_PROJECTS` are set,
this gathers the extra artifacts found in repos relevant repositories,
and makes them available to `aptly-plus`

#### aptly-plus

This is a minor modification of the normal `aptly` job,
which is able to gather additional artifacts
from the `harvest-repos` job.

### What happens after the D-I trigger

A _debian-installer_ Downstream "child" pipeline gets triggered,
which uses the aptly repo. while building a mini.iso.

The mini.iso has the URL & key of the aptly repo built in,
so is able to download udebs from it.

This is achieved using the
**INTENDED FOR DEVELOPMENT TESTING ONLY**
debconf variables: _apt-setup/_DEVEL_/repository_, _.../key_ & _.../comment_.

These are set in the generated image by
[creating a preseed file]( https://salsa.debian.org/installer-team/debian-installer/-/blob/b2328bbbb5d5a712adb1c11e91e675c2a82c915d/debian/salsa-ci.yml#L99-L104)
containing values furnished by gitlab variables created in the `aptly` job
(_$APTLY_REPO_ & _$APTLY_REPO_KEY_BASE64_).
The `D-I` job then includes that as `/preseed.cfg` in the generated initrd.

## How to make manual jobs in the pipeline run

By default, for `main` or `master` branches
the _D-I_ trigger job in your project's pipeline
will be set to `manual`,
which means that it will not run automatically.
You can make it run via the web-UI in the pipeline's page,
by clicking on the play button in the trigger job.

If you want to make it run automatically,
you can do so by setting one of a couple of variables.
Variables can be set in various ways:

  * by running the pipeline via the web-UI, and setting a variable there
  * by specifying then via git options when you push a commit
  * by adding them to your salsa-ci.yml

The last option should generally be avoided,
unless you know people are likely to use the generated mini.iso.

I quite often use [push options](https://docs.gitlab.com/ee/user/project/push_options.html)
when trying to get a feature branch to work, either like this:

```
git push --force-with-lease -o ci.variable="BRANCH2REPO_DISABLE_MAKEISO=1" -o ci.variable="SALSA_CI_DISABLE_APTLY=1" philh
```

if the udeb is part of a group where other(s) still need to be pushed
in order to make a useful mini-ISO
(BTW the `philh` is there to make sure that a new branch
gets pushed into my namespace,
rather than dropping it into the install-team's),
or this:

```
git push --force-with-lease -o ci.variable="OQA_JOBSTOCLONE=nonGUI@uefi Debian%20(amd64)/kde@amd64 181897" philh
```

when there are one or more openQA jobs that
provide a good starting point for a test.
Setting `OQA_JOBSTOCLONE` causes the pipeline to run,
clone the listed openQA job(s),
which are then run using the newly-built ISO created from the current branch.

In the example above, when one specifies a job by name,
the last successful run of the job is used,
so if you want to show a failure succeeding,
specify the exact job to clone by job number.
If you leave out the `<group>/` bit, it tries
to fill it in as the appropriate Debian group.

## Optional one-time setup:

### Fork [openqa-link](https://salsa.debian.org/philh/openqa-link/)

This enables one to trigger openQA tests of created mini-ISO images.
There are instructions in openqa-link's README.md
for how your fork should be configured to make this work.

## Contributing to _branch2repo_

In order to test out contributions,
one generally wants to try things out in a fork.
That may mean that one needs to get the `D-I` job to
trigger the job in your fork, rather than the default location.

The way that the files are included is somewhat complicated,
so you should search your fork for the string `installer-team`
and replace that in most/all places with your own namespace
in order to actually test your own changes
(rather than find yourself still running the files from the upstream repo).

BTW It can be helpful to be able to specify a particular branch to use,
when invoking branch2repo via the `CI/CD configuration file` setting,
which one can do by adding the branch after a colon (:), thus:
e.g.: `trigger_b2r.yml@installer-team/branch2repo:testing`

<hr>

## DEPRECATED

### update-b2r-state

_this approach has proven less useful than initially thought,
and adds a lot of complication,
so I'm very likely to remove it in future,
unless people tell me it's really useful to them_
(note that `BRANCH2REPO_EXTRA_PROJECTS` allows you
to achieve the same results, in a more understandable
and deterministic manner)

If `BRANCH2REPO_STATEFUL=1` is set
this will trigger a job in the `salsa-branch2repo-state` repo
in your (the triggering user's) namespace.
Which adds the current repo to the matching branch's state
so that it will be included in other stateful builds.

#### [ DEPRECATED ]  Optional one-time setup:

##### Fork [salsa-branch2repo-state](https://salsa.debian.org/installer-team/salsa-branch2repo-state/)

_I may well deprecate the use of salsa-branch2repo-state at some point,
as it's probably easier to explicitly list the additional repos
in `BRANCH2REPO_EXTRA_PROJECTS` in ones's `salsa-ci.yml` instead
-- please tell me if you currently make use of this_

You only need to do this when making changes
to multiple repositories at once,
where you want to be able to modify things in any order
and test all the most recent changes together.
If you just want to use branch2repo
for testing changes to a single repository,
you don't need to worry about this.
If you're mostly just testing one thing,
but that happens to depend upon unreleased changes elsewhere,
you also don't need to do this (use BRANCH2REPO_EXTRA_PROJECTS)

Look at the
[README](https://salsa.debian.org/installer-team/salsa-branch2repo-state/-/blob/main/README.md)
for details of how to setup a per-user repository
that is needed to store the state of branches
that you want to build.

Having done that, ensure that you set `BRANCH2REPO_STATEFUL=1`
in the various projects/branches
that you want to combine into one APT repo.
since otherwise branch2repo will not bother to record the state,
and you will get a simple APT repo.
containing just the results from the triggered build.
The udebs that this stateful approach will find
need to be in the same namespace
(i.e. they're all 'installer-team' repos,
or all under your own user's namespace),
and since each build inserts just its own name into the state
you need to have built each one at least once
to end up with an aptly repo. that includes the full set.

